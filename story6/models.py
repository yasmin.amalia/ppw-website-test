from django.db import models

# Create your models here.
class Status (models.Model):
    name = models.CharField(max_length=20)
    status = models.CharField(max_length=300)
    time = models.DateTimeField(auto_now_add=True)