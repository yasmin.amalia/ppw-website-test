from django.shortcuts import render
from .forms import Status_Form
from .models import Status

# Create your views here.
response = {}
def index(request):
    response['status_form'] = Status_Form
    the_status = Status.objects.all()
    response['Status_Form'] = the_status

    form = Status_Form(request.POST or None)
    if(request.method == 'POST'  and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['status'] = request.POST['status']
    
        the_status = Status(name=response['name'], status=response['status'])
        the_status.save()
    
        return render(request, 'hello.html', response)
    else:
        return render(request, 'hello.html', response)

def profile(request):
    return render(request, 'profile.html')