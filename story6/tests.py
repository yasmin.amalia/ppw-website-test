from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Status
from .forms import Status_Form

# Create your tests here.
class Lab6UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_landing_page_content_is_written(self):
        response = self.client.get('/')
        string = response.content.decode("utf-8")
        self.assertIn("Hello, Apa Kabar?", string)

    def test_model_can_create_new_status(self):
        #Creating a new status
        new_status = Status.objects.create(name='Yasmin', status='ppw bikin stress')

        #Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'name': '', 'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

    def test_url_profile_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_content_is_written(self):
        response = self.client.get('/profile/')
        string = response.content.decode("utf-8")
        self.assertIn("Yasmin Amalia.", string)