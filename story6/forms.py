from django import forms

class Status_Form(forms.Form):
    error_message = {
        'required': 'Please fill in'
    }

    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Your Name'
    }
    stat_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': "What's on your mind?"
    }

    name = forms.CharField(label='Name', required=True, max_length=50,
    widget=forms.TextInput(attrs=name_attrs))
    status = forms.CharField(label='Status', required=True, max_length=300,
    widget= forms.TextInput(attrs=stat_attrs))